# BIND服务

## bind主配置文件

示例：配置文件，原文注释已删除，做出修改的地方会在后面标记有注释

```shell
[root@harbor named]# vim /etc/named.conf
options {
        listen-on port 53 { 10.250.1.200; };	#将监听地址改为内网地址10.250.1.200
        listen-on-v6 port 53 { ::1; };			#监听IPv6地址的选项用不到可删除
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
        allow-query     { any; };	#允许指定的客户端能够通过此DNS进行域名解析，原本为localhost
        forwarders		{ 10.250.1.254; };		#指定上一级DNS服务器的IP，本机无法解析的域名会交给上一级进行解析
        
        recursion yes;				#使用递归算法提供域名解析

        dnssec-enable no;			#这里要把yes改为no，否则解析失败
        dnssec-validation no;

        bindkeys-file "/etc/named.root.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

示例：正向解析配置文件

```shell
[root@harbor named]# cat /var/named/example.com.zone
$TTL 1D
@       IN SOA  harbor.example.com. root.harbor.example.com. (
                                        0       ; serial
                                        1D      ; refresh
                                        1H      ; retry
                                        1W      ; expire
                                        3H )    ; minimum
                NS      harbor.example.com.
harbor          A       10.250.1.200
docker1        A       10.250.1.15
docker2        A       10.250.1.16
docker3        A       10.250.1.17
docker4        A       10.250.1.18
etcd1          A       10.250.1.19
etcd2          A       10.250.1.20
etcd3          A       10.250.1.21
loadbase1      A       10.250.1.22
loadbase2      A       10.250.1.23
```

示例：检测dns服务是否运行正常

```shell
[root@harbor ~]# named-checkconf			#检查主配置文件语法是否正确
[root@harbor ~]# named-checkzone example.com.zone /var/named/example.com.zone	#检查解析文件语法是否正确
[root@harbor ~]# dig -t A docker1.example.com @10.250.1.200 +short		#测试dns服务解析
```

> 补充：配置bind服务过程中除了配置文件严格的语法格式，还需要注意文件所属用户和组

# ISCSI服务

iSCSI（Internet SCSI）小型计算机系统接口，别称IP-SAN，用于在IP网络上运行SCSI协议，解决了SCSI需要直连存储设备的局限性，可以不停机扩展存储容量。iSCSI将SCSI接口与Ethernet技术结合，使服务器可与使用IP网络的存储设备互相交换数据，基于TCP/IP协议，以数据块级别在多个数据存储网络间进行传输

## iSCSI传输过程

iSCSI基于TCP/IP网络接收和传输存储数据，发送端将SCSI命令和数据封装到TCP/IP包中，通过IP网络转发，接收端解析收到的TCP/IP包，还原并执行SCSI命令和数据，执行完成后将返回的SCSI命令和数据封装到TCP/IP包中回传给发送端。iSCSI的整个数据传输过程对用户来说是透明的，用户就像使用本地磁盘一样使用远程的iSCSI存储

iSCSI的两个概念：*initiator和target*，initiator代表主机系统、target代表存储设备。以CentOS7为例，在安装iSCSI时，通过安装的软件包名就能够判断出这台主机是使用者还是服务端。*scsi-target-utils*即服务端，要向外提供硬盘，*iscsi-initiator-utils*即使用者，使用远程iSCSI存储

### iSCSI Initiator

initiator也叫启动器，是客户端设备，initiator可以通过两种方式实现

1. 软件

        即*iscsi-initiator-utils*软件，initiator软件能够将以太网卡虚拟成iSCSI卡，从而与iSCSI服务端进行数据传输。这种方式只需要以太网卡就能使用，因此成本最低，但iSCSI报文和TCP/IP报文转换需要小号服务器的CPU资源，只有在低I/O和低带宽要求的应用环境才能使用这种方式

2. 硬件

        iSCSI HBA（Host Bus Adapter）即initiator硬件，HBA卡即不需要消耗CPU资源，也能够基于硬件提供更好的数据传输和存储性能

## iSCSI安装

### 服务端

